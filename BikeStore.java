//By Xuanru Yang, 1942014
package code;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycle = new Bicycle[4];
        bicycle[0] = new Bicycle("Specialized", 21, 40.1);
        bicycle[1] = new Bicycle("Specialized", 22, 40.2);
        bicycle[2] = new Bicycle("Specialized", 23, 40.3);
        bicycle[3] = new Bicycle("Specialized", 24, 40.4);
        for (int n = 0; n < 4; n++) {
            System.out.println(bicycle[n].toString());
        }
    }

}
