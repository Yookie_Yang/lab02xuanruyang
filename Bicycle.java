//By Xuanru Yang, 1942014
package code;
public class Bicycle {
    public static void main(String[] args) {
        
    } 
    
    private String manufactuer;

    private int numberGears;

    private double maxSpeed;

    public String getManufactuer() {
        return manufactuer;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public Bicycle(String manufactuer, int numberGears, double maxSpeed) {
        this.manufactuer = manufactuer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Manufactuer: " + this.manufactuer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}